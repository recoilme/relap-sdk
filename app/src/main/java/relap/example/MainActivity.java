package relap.example;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import ru.surf.relap_recommenrations.Relap;
import ru.surf.relap_recommenrations.RelapClickListener;
import ru.surf.relap_recommenrations.RelapRequestListener;
import ru.surf.relap_recommenrations.view.RelapSimilarSitesLayout;

public class MainActivity extends Activity implements RelapClickListener {

    private final static String URL = "http://surfingbird.ru/surf/facebook-i-twitter-stali-krupnejshimi-istochnikami--sIfd1808a";
    private final static String API_KEY = "-mjeOg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Relap.registerHit(API_KEY, this, URL,
                new RelapRequestListener() {
                    @Override
                    public void onSuccess() {
                        makeToast("registerHit success");
                    }

                    @Override
                    public void onError(String errorMessage) {
                        makeToast(errorMessage);
                    }
                });
        RelapSimilarSitesLayout mRelapSimilarLayout = (RelapSimilarSitesLayout) findViewById(R.id.RelapSimilarSites);
        mRelapSimilarLayout.setSimilarSiteClickListener(this);

        mRelapSimilarLayout.loadSite(URL, API_KEY);

    }

    @Override
    public void onRecomendationClick(String siteUrl) {
        makeToast("onRecomendationClick");
    }

    public void makeToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
