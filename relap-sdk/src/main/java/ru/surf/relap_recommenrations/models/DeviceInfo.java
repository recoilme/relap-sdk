package ru.surf.relap_recommenrations.models;

/**
 * Created by DariaEfimova on 29.01.16.
 */
public class DeviceInfo {
    private String deviceType;// optional
    private String osVersion;//optional
    private String mobileOperator;//optional
    private String screenResolution;//optional
    private String deviceId;//required

    public DeviceInfo(String _deviceType, String _osVersion, String _mobileOperator,
                      String _screenResolution, String _deviceId) {
        deviceType = _deviceType;
        osVersion = _osVersion;
        mobileOperator = _mobileOperator;
        screenResolution = _screenResolution;
        deviceId = _deviceId;

    }

    public String getDeviceType() {
        return deviceType;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getMobileOperator() {
        return mobileOperator;
    }

    public String getScreenResolution() {
        return screenResolution;
    }

    public String getDeviceId() {
        return deviceId;
    }
}
