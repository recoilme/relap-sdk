package ru.surf.relap_recommenrations.models;

import java.util.List;

/**
 * Created by DariaEfimova on 29.01.16.
 */
public class RecommendationResponse extends BaseApiResponse {

   private Meta meta;
   private List<SimilarSite> recs;

    public Meta getMeta() {
        return meta;
    }

    public List<SimilarSite> getRecs() {
        return recs;
    }

    public class Meta {
        private int count;
        private String rgid;

        public int getCount() {
            return count;
        }

        public String getRgId() {
            return rgid;
        }
    }
}
