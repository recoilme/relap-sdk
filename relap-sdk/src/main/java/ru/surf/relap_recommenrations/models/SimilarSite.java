package ru.surf.relap_recommenrations.models;

/**
 * Describes similar site structure
 */
public class SimilarSite {
    private String type;
    private String contentID;
    private String title;
    private String description;
    private String imageUrl;
    private String url;
    private String rid;
    private RecommendationResponse.Meta meta;

    public String getType() {
        return type;
    }

    public String getContentID() {
        return contentID;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getRid() {
        return rid;
    }

    public RecommendationResponse.Meta getMeta() {
        return meta;
    }

    public String getUrl() {
        return url;
    }
}