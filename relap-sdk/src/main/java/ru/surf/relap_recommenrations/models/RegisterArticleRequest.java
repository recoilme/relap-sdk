package ru.surf.relap_recommenrations.models;

/**
 * Created by DariaEfimova on 29.01.16.
 */
public class RegisterArticleRequest {
    private DeviceInfo deviceInfo;//required
    private String contentId;//optional
    private String url;//optional
    private String rgid;//optional
    private String rid;//optional

    public RegisterArticleRequest(DeviceInfo _deviceInfo, String _url) {
        deviceInfo = _deviceInfo;
        url = _url;
        contentId = _url;
    }

    public RegisterArticleRequest(DeviceInfo _deviceInfo, String _url, String _rgid) {
        deviceInfo = _deviceInfo;
        rgid = _rgid;
        url = _url;
        contentId = _url;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
}
