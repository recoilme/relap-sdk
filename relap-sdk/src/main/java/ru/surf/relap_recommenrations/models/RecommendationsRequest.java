package ru.surf.relap_recommenrations.models;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import ru.surf.relap_recommenrations.api.RelapUtilsApi;

/**
 * Created by DariaEfimova on 29.01.16.
 */
public class RecommendationsRequest {

    public enum IllustrationType {
        STANDART("standard"),
        WIDE("wide"),
        ULTRAWIDE("ultraWide"),
        SQUARE("square"),
        VERTICAL("vertical");

        private String name;

        IllustrationType(String _name) {
            name = _name;
        }

        public String getName() {
            return name;
        }
    }

    DeviceInfo deviceInfo;//required
    int limit;//required
    String illustrationType;
    String url;//optional
//    IllustrationType illustrationType;//required

    public RecommendationsRequest(DeviceInfo _deviceInfo, int _limit, IllustrationType _illustrationType,
                                  String _url) {
        deviceInfo = _deviceInfo;
        limit = _limit;
        if(_illustrationType == IllustrationType.VERTICAL) {
            illustrationType = IllustrationType.ULTRAWIDE.getName();//_illustrationType.getName();
        }else{
            illustrationType = _illustrationType.getName();
        }
        url = _url;
    }
}
