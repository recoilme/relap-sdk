package ru.surf.relap_recommenrations;

import java.util.List;

import ru.surf.relap_recommenrations.models.SimilarSite;

public interface RelapRecomendationLoadListener {

    public void onRecomendationLoaded(List<SimilarSite> sites, String rgId);

    public void onRecomendationNotLoaded(Throwable t);
}