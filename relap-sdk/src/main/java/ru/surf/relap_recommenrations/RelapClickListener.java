package ru.surf.relap_recommenrations;

public interface RelapClickListener {

    public void onRecomendationClick(String siteUrl);
}