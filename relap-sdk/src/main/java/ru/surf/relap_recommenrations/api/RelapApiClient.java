package ru.surf.relap_recommenrations.api;

import android.content.Context;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import ru.surf.relap_recommenrations.RelapRecomendationLoadListener;
import ru.surf.relap_recommenrations.RelapRequestListener;
import ru.surf.relap_recommenrations.models.BaseApiResponse;
import ru.surf.relap_recommenrations.models.RecommendationResponse;
import ru.surf.relap_recommenrations.models.RecommendationsRequest;
import ru.surf.relap_recommenrations.models.RegisterArticleRequest;

/**
 * Created by DariaEfimova on 29.01.16.
 */
public final class RelapApiClient {

    private final String apiKey;
    private final RelapApiService service;

    private RelapApiClient(String _apiKey, RelapApiService _service) {
        apiKey = _apiKey;
        service = _service;
    }

    public static RelapApiClient newInstance(String _apiKey) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://relap.io")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RelapApiService _service = retrofit.create(RelapApiService.class);
        return new RelapApiClient(_apiKey, _service);
    }

    //Async method
    public void registerClick(final Context _context, String _url, String rid, final RelapRequestListener listener) {
        if (service != null) {
            RegisterArticleRequest clickRequest = new RegisterArticleRequest(
                    RelapUtilsApi.generateDeviceInfo(_context), _url);
            clickRequest.setRid(rid);
            Call<BaseApiResponse> request = service.registerClick(apiKey, clickRequest);
            request.enqueue(new Callback<BaseApiResponse>() {
                @Override
                public void onResponse(Response<BaseApiResponse> response) {
                    if (listener != null) {
                        if (response.isSuccess()) {
                            listener.onSuccess();
                        } else {
                            listener.onError(response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    listener.onError(t.getLocalizedMessage());
                }
            });
        }
    }

    //Async method
    public void registerView(final Context _context, String _url, String rgId, final RelapRequestListener listener) {
        if (service != null) {
            Call<BaseApiResponse> request = service.registerView(apiKey,
                    new RegisterArticleRequest(RelapUtilsApi.generateDeviceInfo(_context), _url, rgId));
            request.enqueue(new Callback<BaseApiResponse>() {
                @Override
                public void onResponse(Response<BaseApiResponse> response) {
                    if (listener != null) {
                        if (response.isSuccess()) {
                            listener.onSuccess();
                        } else {
                            listener.onError(response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    listener.onError(t.getLocalizedMessage());
                }
            });
        }
    }

    //Async method
    public void hitArticle(final Context _context, String _url, final RelapRequestListener listener) {
        if (service != null) {
            Call<BaseApiResponse> request = service.hitrArticle(apiKey,
                    new RegisterArticleRequest(RelapUtilsApi.generateDeviceInfo(_context), _url));
            request.enqueue(new Callback<BaseApiResponse>() {
                @Override
                public void onResponse(Response<BaseApiResponse> response) {
                    if (listener != null) {
                        if (response.isSuccess()) {
                            listener.onSuccess();
                        } else {
                            listener.onError(response.message());
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    listener.onError(t.getLocalizedMessage());
                }
            });
        }
    }

    //Async method
    public void getRecomendations(final Context _context, String _url, int limit,
                                  RecommendationsRequest.IllustrationType _illustrationType,
                                  final RelapRecomendationLoadListener lisneter) {
        if (service != null) {
            Call<RecommendationResponse> request = service.getRecommendations(
                    apiKey,
                    new RecommendationsRequest(RelapUtilsApi.generateDeviceInfo(_context), limit, _illustrationType, _url));
            request.enqueue(new Callback<RecommendationResponse>() {
                @Override
                public void onResponse(Response<RecommendationResponse> response) {
                    RecommendationResponse responseBody = response.body();
                    if (lisneter != null && responseBody != null) {
                        String rgId = responseBody.getMeta() != null ? responseBody.getMeta().getRgId() : null;
                        lisneter.onRecomendationLoaded(responseBody.getRecs(), rgId);
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    if (lisneter != null) {
                        lisneter.onRecomendationNotLoaded(t);
                    }
                }
            });
        }
    }
}