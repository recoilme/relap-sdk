package ru.surf.relap_recommenrations.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.surf.relap_recommenrations.models.BaseApiResponse;
import ru.surf.relap_recommenrations.models.RecommendationResponse;
import ru.surf.relap_recommenrations.models.RecommendationsRequest;
import ru.surf.relap_recommenrations.models.RegisterArticleRequest;

/**
 * Created by DariaEfimova on 29.01.16.
 */
public interface RelapApiService {

    @POST("/rest/v1/hit/")
    Call<BaseApiResponse> hitrArticle(@Query("api_key") String apiKey, @Body RegisterArticleRequest request);

    @POST("/rest/v1/recommendations/")
    Call<RecommendationResponse> getRecommendations(@Query("api_key") String apiKey, @Body RecommendationsRequest request);

    @POST("/rest/v1/view/")
    Call<BaseApiResponse> registerView(@Query("api_key") String apiKey, @Body RegisterArticleRequest request);

    @POST("/rest/v1/click/")
    Call<BaseApiResponse> registerClick(@Query("api_key") String apiKey, @Body RegisterArticleRequest request);
}