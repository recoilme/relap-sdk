package ru.surf.relap_recommenrations.api;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import java.util.UUID;

import ru.surf.relap_recommenrations.models.DeviceInfo;

/**
 * Created by recoil on 09.06.14.
 */
public class RelapUtilsApi {

    public static String generateDeviceId(Context context) {
        final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(

                context.getContentResolver(), android

                        .provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());

        return deviceUuid.toString();
    }

    public static DeviceInfo generateDeviceInfo(Context _context) {
        TelephonyManager tManager = (TelephonyManager) _context
                .getSystemService(Context.TELEPHONY_SERVICE);
        String carrierName = tManager.getNetworkOperatorName();
        String model = Build.MODEL;
        DisplayMetrics displayMetrics = _context.getResources().getDisplayMetrics();
        String screenResolution = String.valueOf(displayMetrics.widthPixels) + 'x' +
                String.valueOf(displayMetrics.heightPixels);
        return new DeviceInfo(model, String.valueOf(Build.VERSION.SDK_INT), carrierName,
                screenResolution, RelapUtilsApi.generateDeviceId(_context));
    }
}
