package ru.surf.relap_recommenrations;

public interface RelapRequestListener {

    public void onSuccess();
    public void onError(String errorMessage);
}