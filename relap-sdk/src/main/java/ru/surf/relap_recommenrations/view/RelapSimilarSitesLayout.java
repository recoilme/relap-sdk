package ru.surf.relap_recommenrations.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.surf.relap_recommenrations.R;
import ru.surf.relap_recommenrations.RelapClickListener;
import ru.surf.relap_recommenrations.RelapRecomendationLoadListener;
import ru.surf.relap_recommenrations.RelapRequestListener;
import ru.surf.relap_recommenrations.api.RelapApiClient;
import ru.surf.relap_recommenrations.models.RecommendationsRequest;
import ru.surf.relap_recommenrations.models.SimilarSite;

public class RelapSimilarSitesLayout extends RelativeLayout {

    RelapClickListener mSimilarSiteClickListener;

    ViewGroup left;
    ViewGroup right;

    Context mContext;
    private boolean isViewRegister = false;
    private String rgId;
    private String urlSite;
    private RelapApiClient apiClient;

    private boolean isOneColumn;

    public RelapSimilarSitesLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RelapSimilarSitesLayout);

        int value = a.getInteger(R.styleable.RelapSimilarSitesLayout_illustration_type, 0);

        isOneColumn = value == 1 ? false : true;

        a.recycle();
    }

    public RelapSimilarSitesLayout(Context context) {
        super(context);
        mContext = context;
        init();
    }

    @SuppressLint("NewApi")
    private void init() {
        LayoutInflater.from(mContext).inflate(R.layout.similar_sites_relap_layout, this, true);
        initViews();
    }

    private void initViews() {
        left = (ViewGroup) findViewById(R.id.leftColumn);
        right = (ViewGroup) findViewById(R.id.rightColumn);

        final ViewTreeObserver vto = ((Activity) getContext()).getWindow().getDecorView().getRootView().getViewTreeObserver();
        vto.addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (isShown() && !isViewRegister) {
                    if (apiClient != null && urlSite != null && rgId != null) {
                        apiClient.registerView(getContext(), urlSite, rgId, new RelapRequestListener() {
                            @Override
                            public void onSuccess() {
//                    Log.d("RELAP", "registerView success");
                            }

                            @Override
                            public void onError(String errorMessage) {
//                    Log.d("RELAP", errorMessage);
                            }
                        });
                        isViewRegister = true;
                    }
                }
            }
        });
    }


    public void setSimilarSiteClickListener(RelapClickListener similarSiteClickListener) {
        this.mSimilarSiteClickListener = similarSiteClickListener;
    }


    private View getSimilarView(final SimilarSite site) {
        View child = LayoutInflater.from(mContext).inflate(R.layout.relap_similar_site_row, null, false);

        ImageView thumbImage;
        TextView siteTitle;

        siteTitle = (TextView) child.findViewById(R.id.siteTitle);
        thumbImage = (ImageView) child.findViewById(R.id.siteImage);


        String imageUrl = site.getImageUrl().replace("//", "https://");
        Picasso.with(getContext()).load(imageUrl).into(thumbImage);

        siteTitle.setText(site.getTitle());

        child.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (apiClient != null) {
                    apiClient.registerClick(getContext(), urlSite, site.getRid(), new RelapRequestListener() {
                        @Override
                        public void onSuccess() {
//                    Log.d("RELAP", "registerClick success");
                        }

                        @Override
                        public void onError(String errorMessage) {
//                    Log.d("RELAP", errorMessage);
                        }
                    });
                }
                if (mSimilarSiteClickListener != null) {
                    mSimilarSiteClickListener.onRecomendationClick(site.getUrl());
                }
            }
        });
        return child;
    }

    public void onRefresh(List<SimilarSite> res) {
        left = (ViewGroup) findViewById(R.id.leftColumn);
        right = (ViewGroup) findViewById(R.id.rightColumn);
        left.removeAllViews();
        right.removeAllViews();
        showSimilarSites(res);
    }


    public void loadSite(String _urlSite, String _apiKey) {
        if (apiClient == null) {
            apiClient = RelapApiClient.newInstance(_apiKey);
        }
        urlSite = _urlSite;
        apiClient.getRecomendations(getContext(), _urlSite, 15,
                isOneColumn ? RecommendationsRequest.IllustrationType.VERTICAL : RecommendationsRequest.IllustrationType.SQUARE,
                new RelapRecomendationLoadListener() {
                    @Override
                    public void onRecomendationLoaded(List<SimilarSite> sites, String _rgId) {
                        rgId = _rgId;
                        onRefresh(sites);
                    }

                    @Override
                    public void onRecomendationNotLoaded(Throwable t) {
                    }
                });
    }

    public void showSimilarSites(final List<SimilarSite> sites) {
        if (sites.size() == 0)
            return;

        left.removeAllViews();
        right.removeAllViews();
        View child;
        for (int i = 0; i < sites.size(); i++) {
            child = getSimilarView(sites.get(i));
            if (isOneColumn) {
                left.addView(child);
            } else {
                if (left.getChildCount() <= right.getChildCount()) {
                    left.addView(child);
                } else {
                    right.addView(child);
                }
            }
        }
        if (isOneColumn) {
            left.setVisibility(View.VISIBLE);
            findViewById(R.id.center).setVisibility(View.GONE);
            right.setVisibility(View.GONE);
        } else {
            left.setVisibility(View.VISIBLE);
            right.setVisibility(View.VISIBLE);
        }
        left.invalidate();
        right.invalidate();
    }


    public void clear() {
        left.setVisibility(View.INVISIBLE);
        right.setVisibility(View.INVISIBLE);
    }
}