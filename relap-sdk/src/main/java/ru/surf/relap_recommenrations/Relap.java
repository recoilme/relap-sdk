package ru.surf.relap_recommenrations;

import android.content.Context;

import ru.surf.relap_recommenrations.api.RelapApiClient;

/**
 * Created by DariaEfimova on 29.01.16.
 * <p>
 * Основной класс SDK для сбора статистики (отправляем hit)
 */
public final class Relap {
    /*
    Метод для регистрации активити(статьи) в sdk relap
    метод просто регистрирует hit статьи
     */
    public static void registerHit(String _apiKey, final Context _context, final String _url, RelapRequestListener listener) {
        final RelapApiClient apiClient = RelapApiClient.newInstance(_apiKey);
        apiClient.hitArticle(_context, _url, listener);
    }
}